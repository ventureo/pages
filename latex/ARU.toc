\selectlanguage *{russian}
\contentsline {chapter}{\numberline {1}Содержание}{2}{chapter.1}%
\contentsline {section}{\numberline {1.1}Настройка pacman}{2}{section.1.1}%
\contentsline {subsection}{\numberline {1.1.1}Основные команды для управления пакетами}{2}{subsection.1.1.1}%
\contentsline {subsubsection}{\numberline {1.1.1.1}Пару слов об AUR помощниках}{3}{subsubsection.1.1.1.1}%
\contentsline {subsection}{\numberline {1.1.2}Обновление ключей Arch Linux}{3}{subsection.1.1.2}%
\contentsline {subsection}{\numberline {1.1.3}Включение 32\sphinxhyphen {}битного репозитория}{3}{subsection.1.1.3}%
\contentsline {subsection}{\numberline {1.1.4}Ускорение обновления системы}{4}{subsection.1.1.4}%
\contentsline {subsection}{\numberline {1.1.5}Параллельная загрузка пакетов}{4}{subsection.1.1.5}%
\contentsline {subsection}{\numberline {1.1.6}Отключение таймаутов при загрузке пакетов}{5}{subsection.1.1.6}%
\contentsline {section}{\numberline {1.2}Выбор ПО}{5}{section.1.2}%
\contentsline {subsection}{\numberline {1.2.1}Обязательные к установке пакеты}{5}{subsection.1.2.1}%
\contentsline {subsection}{\numberline {1.2.2}Wayland vs X11}{5}{subsection.1.2.2}%
\contentsline {subsubsection}{\numberline {1.2.2.1}Пакеты для работы с архивами}{8}{subsubsection.1.2.2.1}%
\contentsline {subsubsection}{\numberline {1.2.2.2}Набор прикладного ПО}{8}{subsubsection.1.2.2.2}%
\contentsline {subsubsection}{\numberline {1.2.2.3}Установка Steam}{8}{subsubsection.1.2.2.3}%
\contentsline {subsubsection}{\numberline {1.2.2.4}Piper}{9}{subsubsection.1.2.2.4}%
\contentsline {subsubsection}{\numberline {1.2.2.5}Установка актуальных драйверов для видеокарты}{9}{subsubsection.1.2.2.5}%
\contentsline {paragraph}{\numberline {1.2.2.5.1}NVIDIA}{10}{paragraph.1.2.2.5.1}%
\contentsline {paragraph}{\numberline {1.2.2.5.2}NVIDIA (470xx)}{10}{paragraph.1.2.2.5.2}%
\contentsline {paragraph}{\numberline {1.2.2.5.3}Nouveau (\sphinxstyleemphasis {Только для старых видеокарт})}{10}{paragraph.1.2.2.5.3}%
\contentsline {paragraph}{\numberline {1.2.2.5.4}AMD}{11}{paragraph.1.2.2.5.4}%
\contentsline {paragraph}{\numberline {1.2.2.5.5}Intel}{11}{paragraph.1.2.2.5.5}%
\contentsline {subsection}{\numberline {1.2.3}Удаление лишних пакетов}{11}{subsection.1.2.3}%
\contentsline {subsection}{\numberline {1.2.4}Установка микрокода}{12}{subsection.1.2.4}%
\contentsline {subsection}{\numberline {1.2.5}Установка дополнительных прошивок}{12}{subsection.1.2.5}%
\contentsline {section}{\numberline {1.3}Настройка драйверов GPU}{12}{section.1.3}%
\contentsline {subsection}{\numberline {1.3.1}Настройка закрытого драйвера NVIDIA}{12}{subsection.1.3.1}%
\contentsline {subsubsection}{\numberline {1.3.1.1}Распространенные мифы о настройке драйвера}{13}{subsubsection.1.3.1.1}%
\contentsline {subsubsection}{\numberline {1.3.1.2}Повышение производительности CPU на ноутбках с графикой NVIDIA}{13}{subsubsection.1.3.1.2}%
\contentsline {subsubsection}{\numberline {1.3.1.3}Повышение лимита TDP на ноутбках GPU Ampere и выше}{13}{subsubsection.1.3.1.3}%
\contentsline {subsubsection}{\numberline {1.3.1.4}Специальные переменные окружения}{14}{subsubsection.1.3.1.4}%
\contentsline {subsubsection}{\numberline {1.3.1.5}Добавление драйверов GPU в образы initramfs}{14}{subsubsection.1.3.1.5}%
\contentsline {subsection}{\numberline {1.3.2}Настройка драйверов Mesa}{15}{subsection.1.3.2}%
\contentsline {subsubsection}{\numberline {1.3.2.1}Форсирование использования AMD SAM \sphinxstyleemphasis {(Только для опытных пользователей)}.}{15}{subsubsection.1.3.2.1}%
\contentsline {subsubsection}{\numberline {1.3.2.2}Решение проблем работы графики Vega 11 (Спасибо @Vochatrak\sphinxhyphen {}az\sphinxhyphen {}ezm)}{16}{subsubsection.1.3.2.2}%
\contentsline {subsubsection}{\numberline {1.3.2.3}Многопоточная OpenGL обработка}{16}{subsubsection.1.3.2.3}%
\contentsline {subsubsection}{\numberline {1.3.2.4}Ускорение с помощью OpenCL}{16}{subsubsection.1.3.2.4}%
\contentsline {section}{\numberline {1.4}Гибридная графика в ноутбуках}{17}{section.1.4}%
\contentsline {subsection}{\numberline {1.4.1}Что такое PRIME?}{17}{subsection.1.4.1}%
\contentsline {subsection}{\numberline {1.4.2}Настройка PRIME}{18}{subsection.1.4.2}%
\contentsline {subsubsection}{\numberline {1.4.2.1}Динамическое управление питанием}{18}{subsubsection.1.4.2.1}%
\contentsline {subsection}{\numberline {1.4.3}Использование PRIME Offload}{19}{subsection.1.4.3}%
\contentsline {subsubsection}{\numberline {1.4.3.1}Графический способ}{19}{subsubsection.1.4.3.1}%
\contentsline {paragraph}{\numberline {1.4.3.1.1}Lutris}{20}{paragraph.1.4.3.1.1}%
\contentsline {paragraph}{\numberline {1.4.3.1.2}Steam}{20}{paragraph.1.4.3.1.2}%
\contentsline {paragraph}{\numberline {1.4.3.1.3}Графические окружения}{21}{paragraph.1.4.3.1.3}%
\contentsline {subparagraph}{KDE Plasma}{21}{subparagraph*.4}%
\contentsline {subparagraph}{Cinnamon}{22}{subparagraph*.5}%
\contentsline {subparagraph}{GNOME}{23}{subparagraph*.6}%
\contentsline {subsection}{\numberline {1.4.4}Устранение проблем с PRIME}{23}{subsection.1.4.4}%
\contentsline {subsubsection}{\numberline {1.4.4.1}Внешний монитор сильно тормозит}{23}{subsubsection.1.4.4.1}%
\contentsline {section}{\numberline {1.5}Ускорение загрузки системы}{24}{section.1.5}%
\contentsline {subsection}{\numberline {1.5.1}Ускорение распаковки initramfs}{24}{subsection.1.5.1}%
\contentsline {subsection}{\numberline {1.5.2}Ускорение загрузки системы c помощью systemd}{25}{subsection.1.5.2}%
\contentsline {section}{\numberline {1.6}Настройка служб}{25}{section.1.6}%
\contentsline {subsection}{\numberline {1.6.1}Полезные службы}{25}{subsection.1.6.1}%
\contentsline {subsubsection}{\numberline {1.6.1.1}zram\sphinxhyphen {}generator}{25}{subsubsection.1.6.1.1}%
\contentsline {subsubsection}{\numberline {1.6.1.2}systemd\sphinxhyphen {}oomd}{26}{subsubsection.1.6.1.2}%
\contentsline {subsubsection}{\numberline {1.6.1.3}Ananicy CPP}{26}{subsubsection.1.6.1.3}%
\contentsline {subsubsection}{\numberline {1.6.1.4}TRIM}{27}{subsubsection.1.6.1.4}%
\contentsline {subsubsection}{\numberline {1.6.1.5}irqbalance}{27}{subsubsection.1.6.1.5}%
\contentsline {subsection}{\numberline {1.6.2}Отключение лишних служб}{27}{subsection.1.6.2}%
\contentsline {subsubsection}{\numberline {1.6.2.1}Службы индексирования файлов}{27}{subsubsection.1.6.2.1}%
\contentsline {subsubsection}{\numberline {1.6.2.2}Отключение пользовательских служб GNOME/Cinnamon}{28}{subsubsection.1.6.2.2}%
\contentsline {subsubsection}{\numberline {1.6.2.3}Отключение ненужных служб Plasma}{32}{subsubsection.1.6.2.3}%
\contentsline {section}{\numberline {1.7}Оптимизация пакетов}{35}{section.1.7}%
\contentsline {subsection}{\numberline {1.7.1}Настройка makepkg.conf}{35}{subsection.1.7.1}%
\contentsline {subsubsection}{\numberline {1.7.1.1}Использование tmpfs для сборки в ОЗУ}{35}{subsubsection.1.7.1.1}%
\contentsline {subsubsection}{\numberline {1.7.1.2}Включение ccache}{36}{subsubsection.1.7.1.2}%
\contentsline {subsection}{\numberline {1.7.2}Форсирование использования Clang при сборке пакетов}{37}{subsection.1.7.2}%
\contentsline {subsection}{\numberline {1.7.3}Установка оптимизированных пакетов}{38}{subsection.1.7.3}%
\contentsline {section}{\numberline {1.8}Низкие задержки звука}{40}{section.1.8}%
\contentsline {subsection}{\numberline {1.8.1}PipeWire}{40}{subsection.1.8.1}%
\contentsline {subsubsection}{\numberline {1.8.1.1}Настройка PipeWire}{41}{subsubsection.1.8.1.1}%
\contentsline {paragraph}{\numberline {1.8.1.1.1}Микширование стерео в 5.1}{42}{paragraph.1.8.1.1.1}%
\contentsline {paragraph}{\numberline {1.8.1.1.2}Исправление хрипов под нагрузкой}{42}{paragraph.1.8.1.1.2}%
\contentsline {subsubsection}{\numberline {1.8.1.2}JACK}{42}{subsubsection.1.8.1.2}%
\contentsline {section}{\numberline {1.9}Профилактика диска}{43}{section.1.9}%
\contentsline {subsection}{\numberline {1.9.1}Чистка при помощи Bleachbit}{43}{subsection.1.9.1}%
\contentsline {subsection}{\numberline {1.9.2}Автоматическая очистка кэша pacman}{44}{subsection.1.9.2}%
\contentsline {subsection}{\numberline {1.9.3}Оптимизация баз данных SQLite}{44}{subsection.1.9.3}%
\contentsline {section}{\numberline {1.10}Разгон монитора}{45}{section.1.10}%
\contentsline {subsection}{\numberline {1.10.1}Подготовка}{45}{subsection.1.10.1}%
\contentsline {subsection}{\numberline {1.10.2}Использование редактора wxedid}{45}{subsection.1.10.2}%
\contentsline {section}{\numberline {1.11}Настройка параметров ядра}{48}{section.1.11}%
\contentsline {subsection}{\numberline {1.11.1}Введение}{48}{subsection.1.11.1}%
\contentsline {subsubsection}{\numberline {1.11.1.1}Зачем?}{48}{subsubsection.1.11.1.1}%
\contentsline {subsubsection}{\numberline {1.11.1.2}Виды настроек}{49}{subsubsection.1.11.1.2}%
\contentsline {paragraph}{\numberline {1.11.1.2.1}sysctl}{49}{paragraph.1.11.1.2.1}%
\contentsline {paragraph}{\numberline {1.11.1.2.2}tmpfiles.d}{50}{paragraph.1.11.1.2.2}%
\contentsline {paragraph}{\numberline {1.11.1.2.3}udev}{50}{paragraph.1.11.1.2.3}%
\contentsline {subsection}{\numberline {1.11.2}Оптимизация ввода/вывода}{51}{subsection.1.11.2}%
\contentsline {subsubsection}{\numberline {1.11.2.1}Общие сведения}{51}{subsubsection.1.11.2.1}%
\contentsline {subsubsection}{\numberline {1.11.2.2}Настройка подкачки}{53}{subsubsection.1.11.2.2}%
\contentsline {paragraph}{\numberline {1.11.2.2.1}ZRAM}{54}{paragraph.1.11.2.2.1}%
\contentsline {paragraph}{\numberline {1.11.2.2.2}Отключение упреждающего чтения}{57}{paragraph.1.11.2.2.2}%
\contentsline {subsubsection}{\numberline {1.11.2.3}Алгоритм MGLRU}{58}{subsubsection.1.11.2.3}%
\contentsline {paragraph}{\numberline {1.11.2.3.1}Защита от Page Trashing}{58}{paragraph.1.11.2.3.1}%
\contentsline {subsubsection}{\numberline {1.11.2.4}Настройка грязных страниц}{59}{subsubsection.1.11.2.4}%
\contentsline {subsubsection}{\numberline {1.11.2.5}Настройка кэша VFS}{62}{subsubsection.1.11.2.5}%
\contentsline {subsubsection}{\numberline {1.11.2.6}Настройка планировщиков ввода/вывода}{63}{subsubsection.1.11.2.6}%
\contentsline {subsubsection}{\numberline {1.11.2.7}Увеличение размера карты памяти процесса}{65}{subsubsection.1.11.2.7}%
\contentsline {subsubsection}{\numberline {1.11.2.8}Отключение многоступенчатого включения дисков}{65}{subsubsection.1.11.2.8}%
\contentsline {subsection}{\numberline {1.11.3}Оптимизация работы CPU}{66}{subsection.1.11.3}%
\contentsline {subsubsection}{\numberline {1.11.3.1}Масштабирование частоты CPU}{66}{subsubsection.1.11.3.1}%
\contentsline {paragraph}{\numberline {1.11.3.1.1}Политики масштабирования частоты}{66}{paragraph.1.11.3.1.1}%
\contentsline {paragraph}{\numberline {1.11.3.1.2}Альтернативные драйверы масштабирования}{67}{paragraph.1.11.3.1.2}%
\contentsline {paragraph}{\numberline {1.11.3.1.3}Настройка параметров масштабирования}{68}{paragraph.1.11.3.1.3}%
\contentsline {subsubsection}{\numberline {1.11.3.2}Отключение сторожевых таймеров}{70}{subsubsection.1.11.3.2}%
\contentsline {subsubsection}{\numberline {1.11.3.3}Отключение защиты от уязвимостей (по желанию)}{70}{subsubsection.1.11.3.3}%
\contentsline {section}{\numberline {1.12}Файловые системы}{72}{section.1.12}%
\contentsline {subsection}{\numberline {1.12.1}Нюансы выбора файловой системы и флагов монтирования}{72}{subsection.1.12.1}%
\contentsline {subsection}{\numberline {1.12.2}Обычные системы}{73}{subsection.1.12.2}%
\contentsline {subsubsection}{\numberline {1.12.2.1}Файловая система ext4}{73}{subsubsection.1.12.2.1}%
\contentsline {paragraph}{\numberline {1.12.2.1.1}Настройка внешнего журнала ext4}{73}{paragraph.1.12.2.1.1}%
\contentsline {paragraph}{\numberline {1.12.2.1.2}Оптимальные параметры монтирования для ext4}{73}{paragraph.1.12.2.1.2}%
\contentsline {subsubsection}{\numberline {1.12.2.2}Файловая система XFS}{74}{subsubsection.1.12.2.2}%
\contentsline {subsubsection}{\numberline {1.12.2.3}Файловая система F2FS}{74}{subsubsection.1.12.2.3}%
\contentsline {subsection}{\numberline {1.12.3}Системы CoW}{74}{subsection.1.12.3}%
\contentsline {subsubsection}{\numberline {1.12.3.1}Файловая система btrfs}{74}{subsubsection.1.12.3.1}%
\contentsline {paragraph}{\numberline {1.12.3.1.1}Оптимальные флаги монтирования}{74}{paragraph.1.12.3.1.1}%
\contentsline {paragraph}{\numberline {1.12.3.1.2}Сжатие в файловой системе Btrfs}{76}{paragraph.1.12.3.1.2}%
\contentsline {paragraph}{\numberline {1.12.3.1.3}Определение эффективности сжатия}{77}{paragraph.1.12.3.1.3}%
\contentsline {paragraph}{\numberline {1.12.3.1.4}Скорость обработки алгоритма zstd на примере AMD Ryzen 7 3700X}{77}{paragraph.1.12.3.1.4}%
\contentsline {paragraph}{\numberline {1.12.3.1.5}Список протестированных игр на эффективность сжатия (Спасибо @dewdpol!)}{79}{paragraph.1.12.3.1.5}%
\contentsline {subparagraph}{Промежуточные результаты}{85}{subparagraph*.44}%
\contentsline {subsubsection}{\numberline {1.12.3.2}Файловая система bcachefs}{86}{subsubsection.1.12.3.2}%
\contentsline {section}{\numberline {1.13}Wine / Linux Gaming}{86}{section.1.13}%
\contentsline {subsection}{\numberline {1.13.1}Основные составляющие}{86}{subsection.1.13.1}%
\contentsline {subsubsection}{\numberline {1.13.1.1}Что такое Wine?}{86}{subsubsection.1.13.1.1}%
\contentsline {subsubsection}{\numberline {1.13.1.2}Сборки Wine}{86}{subsubsection.1.13.1.2}%
\contentsline {subsubsection}{\numberline {1.13.1.3}Установка wine\sphinxhyphen {}pure}{86}{subsubsection.1.13.1.3}%
\contentsline {paragraph}{\numberline {1.13.1.3.1}Proton\sphinxhyphen {}GE\sphinxhyphen {}Custom}{87}{paragraph.1.13.1.3.1}%
\contentsline {subsubsection}{\numberline {1.13.1.4}Использование Wine}{88}{subsubsection.1.13.1.4}%
\contentsline {paragraph}{\numberline {1.13.1.4.1}Настройка префикса}{89}{paragraph.1.13.1.4.1}%
\contentsline {subparagraph}{Изоляция Wine программ от доступа к файловой системе}{89}{subparagraph*.46}%
\contentsline {subparagraph}{Проблема потери управления при смени фокуса на другое окно}{91}{subparagraph*.47}%
\contentsline {subparagraph}{Исправление чёрных окон лаунчеров на базе .NET}{93}{subparagraph*.48}%
\contentsline {subparagraph}{Исправление чёрных окон лаунчеров на базе CEF}{94}{subparagraph*.49}%
\contentsline {subsubsection}{\numberline {1.13.1.5}DXVK}{94}{subsubsection.1.13.1.5}%
\contentsline {subsubsection}{\numberline {1.13.1.6}vkd3d}{97}{subsubsection.1.13.1.6}%
\contentsline {subsubsection}{\numberline {1.13.1.7}Полезные ссылки по теме Wine и DXVK}{98}{subsubsection.1.13.1.7}%
\contentsline {subsection}{\numberline {1.13.2}Дополнительные компоненты}{99}{subsection.1.13.2}%
\contentsline {subsubsection}{\numberline {1.13.2.1}Lutris}{99}{subsubsection.1.13.2.1}%
\contentsline {subsubsection}{\numberline {1.13.2.2}Gamemode}{105}{subsubsection.1.13.2.2}%
\contentsline {subsubsection}{\numberline {1.13.2.3}Использование DLSS с видеокартами NVIDIA через Proton}{106}{subsubsection.1.13.2.3}%
\contentsline {subsubsection}{\numberline {1.13.2.4}Gamescope}{107}{subsubsection.1.13.2.4}%
\contentsline {paragraph}{\numberline {1.13.2.4.1}Запуск gamescope в отдельном tty}{107}{paragraph.1.13.2.4.1}%
\contentsline {subsubsection}{\numberline {1.13.2.5}Мониторинг FPS в играх.}{108}{subsubsection.1.13.2.5}%
\contentsline {paragraph}{\numberline {1.13.2.5.1}Mangohud}{108}{paragraph.1.13.2.5.1}%
\contentsline {paragraph}{\numberline {1.13.2.5.2}Альтернатива: DXVK Hud (\sphinxstyleemphasis {Только для игр запускаемых через Wine/Proton})}{109}{paragraph.1.13.2.5.2}%
\contentsline {subsubsection}{\numberline {1.13.2.6}Настройка геймпадов Xbox}{109}{subsubsection.1.13.2.6}%
\contentsline {paragraph}{\numberline {1.13.2.6.1}Настройка стандартного xpad}{109}{paragraph.1.13.2.6.1}%
\contentsline {paragraph}{\numberline {1.13.2.6.2}Драйвер xpadneo с поддержкой Bluetooth}{110}{paragraph.1.13.2.6.2}%
\contentsline {section}{\numberline {1.14}Оконный менеджер river}{110}{section.1.14}%
\contentsline {subsection}{\numberline {1.14.1}Конфиг river}{110}{subsection.1.14.1}%
\contentsline {subsection}{\numberline {1.14.2}Настройка river}{111}{subsection.1.14.2}%
\contentsline {subsubsection}{\numberline {1.14.2.1}Назначение клавиш}{111}{subsubsection.1.14.2.1}%
\contentsline {subsubsection}{\numberline {1.14.2.2}Настройка раскладки клавиатуры}{113}{subsubsection.1.14.2.2}%
\contentsline {subsubsection}{\numberline {1.14.2.3}Настройка медиа\sphinxhyphen {}клавиш или устройств}{114}{subsubsection.1.14.2.3}%
\contentsline {subsubsection}{\numberline {1.14.2.4}"Тайлинг" в river}{115}{subsubsection.1.14.2.4}%
\contentsline {paragraph}{\numberline {1.14.2.4.1}Настройка параметров rivertile}{115}{paragraph.1.14.2.4.1}%
\contentsline {paragraph}{\numberline {1.14.2.4.2}Взаимодействие с окнами}{116}{paragraph.1.14.2.4.2}%
\contentsline {paragraph}{\numberline {1.14.2.4.3}Управление размерами окон}{117}{paragraph.1.14.2.4.3}%
\contentsline {subsubsection}{\numberline {1.14.2.5}Настройка tags (рабочих столов)}{118}{subsubsection.1.14.2.5}%
\contentsline {subsubsection}{\numberline {1.14.2.6}Настройка экранов(мониторов)}{119}{subsubsection.1.14.2.6}%
\contentsline {subsubsection}{\numberline {1.14.2.7}Настройка управления программами / приложениями}{122}{subsubsection.1.14.2.7}%
\contentsline {paragraph}{\numberline {1.14.2.7.1}Добавить правило}{122}{paragraph.1.14.2.7.1}%
\contentsline {subparagraph}{Правило определяющее \sphinxstyleemphasis {tag} для программы}{122}{subparagraph*.50}%
\contentsline {subparagraph}{Правило определяющее монитор для программы}{123}{subparagraph*.51}%
\contentsline {subparagraph}{Правило регулирующее отображение программы в полноэкранном режиме}{123}{subparagraph*.52}%
\contentsline {subparagraph}{Правило для плавающего окна}{123}{subparagraph*.53}%
\contentsline {subparagraph}{Правила размера и расположения}{124}{subparagraph*.54}%
\contentsline {subparagraph}{Правило отвечающее за tearing}{124}{subparagraph*.55}%
\contentsline {subparagraph}{Правило указывающее на управляющего по декорациям окна}{124}{subparagraph*.56}%
\contentsline {subparagraph}{Настройка внешнего вида river}{125}{subparagraph*.57}%
\contentsline {subparagraph}{Настройки для курсора}{126}{subparagraph*.58}%
\contentsline {paragraph}{\numberline {1.14.2.7.2}Удаление правил}{126}{paragraph.1.14.2.7.2}%
\contentsline {paragraph}{\numberline {1.14.2.7.3}Просмотр заданных правил}{126}{paragraph.1.14.2.7.3}%
\contentsline {subsubsection}{\numberline {1.14.2.8}Особые режимы / события в river}{127}{subsubsection.1.14.2.8}%
\contentsline {subsubsection}{\numberline {1.14.2.9}Параметры для настройки устройств ввода}{127}{subsubsection.1.14.2.9}%
\contentsline {subsubsection}{\numberline {1.14.2.10}Настройка уведомлений}{131}{subsubsection.1.14.2.10}%
\contentsline {subsubsection}{\numberline {1.14.2.11}Панель (status bar)}{131}{subsubsection.1.14.2.11}%
\contentsline {subsubsection}{\numberline {1.14.2.12}Меню запуска приложений}{132}{subsubsection.1.14.2.12}%
\contentsline {subsubsection}{\numberline {1.14.2.13}Блокировка экрана}{132}{subsubsection.1.14.2.13}%
\contentsline {chapter}{Алфавитный указатель}{133}{section*.59}%
